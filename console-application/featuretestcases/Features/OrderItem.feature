﻿Feature: OrderItem
	As a customer of the Food Delivery System 
	I want to order some items

	#Assume some items are already in the menu like - 
	#Chapati, Idli, Dosa

@order_exists
Scenario Outline: If the wanted items exists in the menu
	Given the role is 1
	And the "<item_choice>" is taken
	When I press order now button
	Then the result should be "Order placed successfully"

	Examples: 
	| item_choice |
	| 1           |
	| 2           |
	| 3           |

@order_not_exists
Scenario Outline: If the wanted item does not exists in the menu
	Given the role is 1
	And the "<item_choice>" is taken
	When I press order now button
	Then the result should be "Item is not available"

	Examples: 
	| item_choice |
	| -1          |
	| 0           |
	| 4           |
	| 5           |
	|             |
	| -10         |
	| 9           |