﻿Feature: ItemHighestRevenue
	As a manager of the food delivery system 
	I want to see the item with the highest revenue
	
	# Let these items already exists in the menu
	#|  item_name  | item_price |
	#|    Chapati  |         30 |
	#|       Idli  |         25 |
	#|       Dosa  |         20 |
	#|     Samosa  |         18 |
	#|    Paratha  |         15 |
	#| Masala Pav  |         35 |
	#|      Laddu  |         10 |
	#|       Puri  |         15 |

@valid_scenario_1
Scenario: If order history contains Chapati, Dosa, Dosa, Idli, Samosa, Paratha, Paratha
	Given the role is 2
	And the choice is 2
	When I press get revenue button
	Then the result should be "Item(s) which contributes most in the revenue is/are Dosa"

@valid_scenario_2
Scenario: If order history contains Samosa, Dosa, Chapati, Chapati, Dosa, Dosa, Puri, Idli
	Given the role is 2
	And the choice is 2
	When I press get revenue button
	Then the result should be "Item(s) which contributes most in the revenue is/are Chapati,Dosa"

@valid_scenario_3
Scenario: If order history contains Chapati, Paratha, Puri, Puri, Paratha, Dosa
	Given the role is 2
	And the choice is 2
	When I press get revenue button
	Then the result should be "Item(s) which contributes most in the revenue is/are Chapati,Paratha,Puri"

@valid_scenario_4
Scenario: If order history contains Dosa, Puri, Laddu, Laddu, Paratha
	Given the role is 2 
	And the choice is 2
	When I press get revenue button
	Then the result should be "Item(s) which contributes most in the revenue is/are Dosa,Laddu"

@invalid_scenario
Scenario: If no item is ordered till now
	Given the role is 2
	And the choice is 2
	When I press get revenue button
	Then the result should be "There is no order till now..."