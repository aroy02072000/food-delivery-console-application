﻿Feature: MostPopularItem
	As a manager of the Food Delivery System
	I want to see the most popular item

	# Let these items already exists in the menu
	#|  item_name  | item_price |
	#|    Chapati  |         30 |
	#|       Idli  |         25 |
	#|       Dosa  |         20 |
	#|     Samosa  |         18 |
	#|    Paratha  |         15 |
	#| Masala Pav  |         35 |
	#|      Laddu  |         10 |
	#|       Puri  |         15 |

@valid_scenario_1
Scenario: If the order history contains Dosa, Chapati, Chapati, Dosa, Dosa, Puri, Paratha
	Given the role is 2
	And the choice is 5
	When I press get popular item button
	Then the result should be "Most popular item(s) is/are Dosa"

@valid_scenario_2
Scenario: If the order history contains Idli, Chapati, Dosa, Idli, Puri, Chapati, Samosa
	Given the role is 2
	And the choice is 5
	When I press get popular item button
	Then the result should be "Most popular item(s) is/are Chapati,Idli"

@valid_scenario_3
Scenario: If the order history contains Idli, Dosa, Samosa, Dosa, Puri, Idli, Chapati, Chapati, Laddu
	Given the role is 2
	And the choice is 5
	When I press get popular item button
	Then the result should be "Most popular item(s) is/are Chapati,Dosa,Idli"

@invalid_scenario
Scenario: If no item is ordered so far
	Given the role is 2
	And the choice is 5
	When I press get popular item button
	Then the result should be "No item is ordered till now!"