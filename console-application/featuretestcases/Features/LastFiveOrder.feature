﻿Feature: LastFiveOrder
	As a manager of the Food Delivery System
	I want to get the last 5 ordered items

	# Let these items already exists in the menu
	#|  item_name  | item_price |
	#|    Chapati  |         30 |
	#|       Idli  |         25 |
	#|       Dosa  |         20 |
	#|     Samosa  |         18 |
	#|    Paratha  |         15 |
	#| Masala Pav  |         35 |
	#|      Laddu  |         10 |
	#|       Puri  |         15 |

@valid_scenario_1
Scenario: If five or more order history exists and let the order history contains Samosa, Puri, Dosa, Idli, Dosa, Chapati, Dosa
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "Last five orders are Dosa,Idli,Dosa,Chapati,Dosa"

@valid_scenario_2
Scenario: If five or more order history exists and let the order history contains Dosa, Idli, Samosa, Paratha, Puri, Dosa, Chapati, Dosa
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "Last five orders are Paratha,Puri,Dosa,Chapati,Dosa"

@valid_scenario_3
Scenario: If five or more order history exists and let the order history contains Puri, Dosa, Paratha, Puri, Chapati
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "Last five orders are Puri,Dosa,Paratha,Puri,Chapati"

@valid_scenario_4
Scenario: If five or more order history exists and let the order history contains Samosa, Puri, Paratha, Idli, Paratha, Dosa, Dosa, Samosa
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "Last five orders are Idli,Paratha,Dosa,Dosa,Samosa"

@valid_scenario_5
Scenario: If five or more order history exists and let the order history contains Dosa, Dosa, Idli, Chapati, Samosa
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "Last five orders are Dosa,Dosa,Idli,Chapati,Samosa"
	
@invalid_scenario
Scenario: If less than five order history exists
	Given the role is 2
	And the choice is 4
	When I press get history button
	Then the result should be "There are less than 5 orders!"
