﻿Feature: ItemBelowPrice
	As a manager of the Food Delivery System
	I want to get the items which are less than 
	the price given by me 

	# Let these items already exists in the menu
	#|  item_name  | item_price |
	#|    Chapati  |         30 |
	#|       Idli  |         25 |
	#|       Dosa  |         20 |
	#|     Samosa  |         18 |
	#|    Paratha  |         15 |
	#| Masala Pav  |         35 |
	#|      Laddu  |         10 |

@item_exists
Scenario Outline: If the item below the given price exists
	Given the role is 2
	And the choice is 3
	And the "<item_price>" is taken
	When I press get items button
	Then the result should be "<list_of_items>"

	Examples: 
	| item_price | list_of_items						                                     |
	| 30         | Item(s) below 30 is/are Idli,Dosa,Samosa,Paratha,Laddu                    |
	| 25         | Item(s) below 25 is/are Dosa,Samosa,Paratha,Laddu                         |
	| 11         | Item(s) below 11 is/are Laddu                                             |
	| 35         | Item(s) below 35 is/are Chapati,Idli,Dosa,Samosa,Paratha,Laddu            |
	| 70         | Item(s) below 70 is/are Chapati,Idli,Dosa,Samosa,Paratha,Masala Pav,Laddu |

@item_does_not_exist
Scenario Outline: If the item below the given price does not exists
	Given the role is 2 
	And the choice is 3
	And the "<item_price>" is taken
	When I press get items button
	Then the result should be "No items are available!"

	Examples: 
	| item_price |
	| 0          |
	| -10        |
	| 10         |
	| 5          |
	|            |