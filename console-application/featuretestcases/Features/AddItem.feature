﻿Feature: AddItem
	As a manager of the Food Delivery System
	I want to add some items and their prices 
	in the menu 

@valid_price
Scenario Outline: Add valid items in the menu
	Given the role is 2
	And the choice is 1
	And the item name "<item_name>" is taken
	And the item price "<item_price>" is taken
	When I press add item button
	Then the result should be "Item added successfully to the menu"

	Examples: 
	| item_name | item_price	|
	| Chapati   | 35			|
	| Dosa      | 20			|
	| Idli      | 25.05         |
	| Samosa    | 10.50         |
	| Puri      | 20.6			|

@invalid_price
Scenario Outline: Add items in the menu with invalid price
	Given the role is 2
	And the choice is 1
	And the item name "<item_name>" is taken
	And the item price "<item_price>" is taken
	When I press add item button
	Then the result should be "Item price is invalid! Cannot be added..."

	Examples: 
	| item_name | item_price |
	| Chapati   | 23a        |
	| Dosa      | -2.33      |
	| Idli      | -30        |
	| Samosa    | -1.2a      |
	| Puri      |            |