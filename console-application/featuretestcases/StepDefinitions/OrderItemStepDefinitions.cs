using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace featuretestcases.StepDefinitions
{
    [Binding, Scope(Feature = "OrderItem")]
    public class OrderItemStepDefinitions
    {
        private string? ItemChoice;
        private string? Status;

        [Given(@"the role is (.*)")]
        public void GivenTheRoleIs(int p0)
        {

        }

        [Given(@"the ""([^""]*)"" is taken")]
        public void GivenTheIsTaken(string p0)
        {
            ItemChoice = p0;
        }

        [When(@"I press order now button")]
        public void WhenIPressOrderNowButton()
        {
            Manager manager = Manager.GetInstance();
            manager.AddItemToMenu("Chapati", "30");
            manager.AddItemToMenu("Idli", "20");
            manager.AddItemToMenu("Dosa", "25");
            Customer customer = new Customer();
            Status = customer.PlaceOrder(ItemChoice);
        }

        [Then(@"the result should be ""([^""]*)""")]
        public void ThenTheResultShouldBe(string status)
        {
            Assert.Equal(status, Status);
            Menu menu = Menu.GetInstance();
            menu.ClearMenu();
            Manager.ClearHistory();
        }
    }
}
