using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace featuretestcases.StepDefinitions
{
    [Binding, Scope(Feature = "LastFiveOrder")]
    public class LastFiveOrderStepDefinitions
    {
        ScenarioContext _scenarioContext;
        private string? ResultSet;

        public LastFiveOrderStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Manager manager = Manager.GetInstance();
            manager.AddItemToMenu("Chapati", "30");
            manager.AddItemToMenu("Idli", "25");
            manager.AddItemToMenu("Dosa", "20");
            manager.AddItemToMenu("Samosa", "18");
            manager.AddItemToMenu("Paratha", "15");
            manager.AddItemToMenu("Masala Pav", "35");
            manager.AddItemToMenu("Laddu", "10");
            manager.AddItemToMenu("Puri", "15");
        }

        [Given(@"the role is (.*)")]
        public void GivenTheRoleIs(int p0)
        {

        }

        [Given(@"the choice is (.*)")]
        public void GivenTheChoiceIs(int p0)
        {

        }

        [When(@"I press get history button")]
        public void WhenIPressGetHistoryButton()
        {
            Manager manager = Manager.GetInstance();
            Customer customer = new Customer();
            string[] items;
            if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_1"))
            {
                items = new string[] { "4", "8", "3", "2", "3", "1", "3" };
                foreach (var item in items)
                {
                    customer.PlaceOrder(item);
                }
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_2"))
            {
                items = new string[] { "3", "2", "4", "5", "8", "3", "1", "3" };
                foreach (var item in items)
                {
                    customer.PlaceOrder(item);
                }
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_3"))
            {
                items = new string[] { "8", "3", "5", "8", "1" };
                foreach (var item in items)
                {
                    customer.PlaceOrder(item);
                }
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_4"))
            {
                items = new string[] { "4", "8", "5", "2", "5", "3", "3", "4" };
                foreach (var item in items)
                {
                    customer.PlaceOrder(item);
                }
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_5"))
            {
                items = new string[] { "3", "3", "2", "1", "4" };
                foreach (var item in items)
                {
                    customer.PlaceOrder(item);
                }
            }
            ResultSet = manager.GetLastFiveOrders();
        }

        [Then(@"the result should be ""([^""]*)""")]
        public void ThenTheResultShouldBe(string resultSet)
        {
            Assert.Equal(resultSet, ResultSet);
            Menu menu = Menu.GetInstance();
            menu.ClearMenu();
            Manager.ClearHistory();
        }
    }
}
