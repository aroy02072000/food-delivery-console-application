using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace featuretestcases.StepDefinitions
{
    [Binding, Scope(Feature = "MostPopularItem")]
    public class MostPopularItemStepDefinitions
    {
        ScenarioContext _scenarioContext;
        public string? ResultSet;

        public MostPopularItemStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Manager manager = Manager.GetInstance();
            manager.AddItemToMenu("Chapati", "30");
            manager.AddItemToMenu("Idli", "25");
            manager.AddItemToMenu("Dosa", "20");
            manager.AddItemToMenu("Samosa", "18");
            manager.AddItemToMenu("Paratha", "15");
            manager.AddItemToMenu("Masala Pav", "35");
            manager.AddItemToMenu("Laddu", "10");
            manager.AddItemToMenu("Puri", "15");
        }

        [Given(@"the role is (.*)")]
        public void GivenTheRoleIs(int p0)
        {

        }

        [Given(@"the choice is (.*)")]
        public void GivenTheChoiceIs(int p0)
        {

        }

        [When(@"I press get popular item button")]
        public void WhenIPressGetPopularItemButton()
        {
            Customer customer = new Customer();
            if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_1"))
            {
                customer.PlaceOrder("3");
                customer.PlaceOrder("1");
                customer.PlaceOrder("1");
                customer.PlaceOrder("3");
                customer.PlaceOrder("3");
                customer.PlaceOrder("8");
                customer.PlaceOrder("5");
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_2"))
            {
                customer.PlaceOrder("2");
                customer.PlaceOrder("1");
                customer.PlaceOrder("3");
                customer.PlaceOrder("2");
                customer.PlaceOrder("8");
                customer.PlaceOrder("1");
                customer.PlaceOrder("4");
            }
            else if (_scenarioContext.ScenarioInfo.Tags.Contains("valid_scenario_3"))
            {
                customer.PlaceOrder("2");
                customer.PlaceOrder("3");
                customer.PlaceOrder("4");
                customer.PlaceOrder("3");
                customer.PlaceOrder("8");
                customer.PlaceOrder("2");
                customer.PlaceOrder("1");
                customer.PlaceOrder("1");
                customer.PlaceOrder("7");
            }
            Manager manager = Manager.GetInstance();
            ResultSet = manager.GetTheMostPopularItem();
        }

        [Then(@"the result should be ""([^""]*)""")]
        public void ThenTheResultShouldBe(string resultSet)
        {
            Assert.Equal(resultSet, ResultSet);
            Menu menu = Menu.GetInstance();
            menu.ClearMenu();
            Manager.ClearHistory();
        }
    }
}
