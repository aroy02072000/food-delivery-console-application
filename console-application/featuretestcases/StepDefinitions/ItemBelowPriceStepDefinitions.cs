using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace featuretestcases.StepDefinitions
{
    [Binding, Scope(Feature = "ItemBelowPrice")]
    public class GetItemsBelowThePriceFeatureStepDefinitions
    {
        private string? Price;
        private string? Status;

        [Given(@"the role is (.*)")]
        public void GivenTheRoleIs(int p0)
        {

        }

        [Given(@"the choice is (.*)")]
        public void GivenTheChoiceIs(int p0)
        {

        }

        [Given(@"the ""([^""]*)"" is taken")]
        public void GivenTheIsTaken(string p0)
        {
            Price = p0;
        }

        [When(@"I press get items button")]
        public void WhenIPressGetItemsButton()
        {
            Manager manager = Manager.GetInstance();
            manager.AddItemToMenu("Chapati", "30");
            manager.AddItemToMenu("Idli", "25");
            manager.AddItemToMenu("Dosa", "20");
            manager.AddItemToMenu("Samosa", "18");
            manager.AddItemToMenu("Paratha", "15");
            manager.AddItemToMenu("Masala Pav", "35");
            manager.AddItemToMenu("Laddu", "10");
            Status = manager.GetItemsBelowThePrice(Price);
        }

        [Then(@"the result should be ""([^""]*)""")]
        public void ThenTheResultShouldBe(string status)
        {
            Assert.Equal(status, Status);
            Menu menu = Menu.GetInstance();
            menu.ClearMenu();
            Manager.ClearHistory();
        }
    }
}
