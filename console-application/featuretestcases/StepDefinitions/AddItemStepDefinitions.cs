using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace featuretestcases.StepDefinitions
{
    [Binding, Scope(Feature = "AddItem")]
    public class AddItemStepDefinitions
    {

        private string? ItemName;
        private string? ItemPrice;
        private string? Status;

        [Given(@"the role is (.*)")]
        public void GivenTheRoleIs(int p0)
        {

        }

        [Given(@"the choice is (.*)")]
        public void GivenTheChoiceIs(int p0)
        {

        }

        [Given(@"the item name ""([^""]*)"" is taken")]
        public void GivenTheItemNameIsTaken(string p0)
        {
            ItemName = p0;
        }

        [Given(@"the item price ""([^""]*)"" is taken")]
        public void GivenTheItemPriceIsTaken(string p0)
        {
            ItemPrice = p0;
        }

        [When(@"I press add item button")]
        public void WhenIPressAddItemButton()
        {
            Manager manager = Manager.GetInstance();
            Status = manager.AddItemToMenu(ItemName, ItemPrice);
        }

        [Then(@"the result should be ""([^""]*)""")]
        public void ThenTheResultShouldBe(string status)
        {
            Assert.Equal(status, Status);
            Menu menu = Menu.GetInstance();
            menu.ClearMenu();
            Manager.ClearHistory();
        }
    }
}