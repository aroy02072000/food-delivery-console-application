﻿namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // Manager is a singleton class contains all the functionalities of a Manager
    public sealed class Manager : IManager
    {

        private static Manager? ObjectExists = null;
        private readonly Menu menu;
        private readonly OrderHistory orderHistory;

        // Get a single instance of the class
        public static Manager GetInstance()
        {
            if(ObjectExists == null)
                ObjectExists = new Manager();
            return ObjectExists;
        }

        private Manager()
        {
            menu = Menu.GetInstance();
            orderHistory = OrderHistory.GetInstance();
        }

        // This method can add an item to the existing menu
        public string AddItemToMenu(string? itemName, string? itemPrice)
        {
            try
            {
                checked
                {
                    if (itemName == null || itemPrice == null || Convert.ToDouble(itemPrice) <= 0)
                        throw new Exception();
                    menu.SetMenuItem(itemName, Convert.ToDouble(itemPrice));
                    return "Item added successfully to the menu";
                }
            }
            catch (Exception)
            {
                return "Item price is invalid! Cannot be added...";
            }
        }

        // This method can retrieve the existing items which have a price below the given price
        public string GetItemsBelowThePrice(string? price)
        {
            try
            {
                checked
                {
                    if (price == null)
                        throw new Exception();
                    IEnumerable<string> itemList = menu.GetMenu()
                                                   .Where(m => m.Value < Convert.ToDouble(price))
                                                   .Select(m => m.Key);
                    if (itemList.Count() == 0)
                        return "No items are available!";
                    else
                        return "Item(s) below " + price + " is/are " + String.Join(',', itemList);
                }
            }
            catch(Exception)
            {
                return "No items are available!";
            }
            
        }

        // This method can retrieve the ordered items which results in the highest revenue
        public string GetItemWithHighestRevenue()
        {
            try
            {
                List<string> orderInfo = orderHistory.GetPastOrderInfo().ToList();
                if (orderInfo.Count() == 0)
                    throw new Exception();
                Dictionary<string, double> orderDetails = menu.GetMenu();
                List<ItemInfo> orderBill = orderInfo.Join(orderDetails, 
                                           a => a, b => b.Key, 
                                           (a, b) => new ItemInfo() 
                                           { ItemName = a, ItemPrice = b.Value}).ToList();
                List<OrderAccounts> orderRevenue = orderBill.GroupBy(t => t.ItemName)
                                                   .Select(o => new OrderAccounts() 
                                                   { ItemName = o.Key, 
                                                   Revenue = o.Sum(t => t.ItemPrice), Qty = o.Count()}).ToList();
                orderRevenue = orderRevenue.OrderByDescending(o => o.Revenue).ToList();
                double maxRevenue = orderRevenue.First().Revenue;
                IEnumerable<string> itemsWithHighestRevenue = orderRevenue.Where(o => o.Revenue == maxRevenue)
                                                              .Select(t => t.ItemName);
                itemsWithHighestRevenue = itemsWithHighestRevenue.OrderBy(t => t);
                return "Item(s) which contributes most in the revenue is/are " + String.Join(',', itemsWithHighestRevenue);
            }
            catch (Exception)
            {
                return "There is no order till now...";
            }
        }

        // This method can retrieve the last 5 ordered items
        public string GetLastFiveOrders()
        {
            try
            {
                Stack<string> orderInfo = orderHistory.GetPastOrderInfo();
                if (orderInfo.Count() < 5)
                    throw new Exception();
                return  "Last five orders are " + String.Join(',', orderInfo.Take(5).Select(t => t).Reverse());
            }
            catch(Exception)
            {
                return "There are less than 5 orders!";
            }

        }

        // This method can retrieve the most frequently ordered items
        public string GetTheMostPopularItem()
        {
            try
            {
                List<string> orderInfo = orderHistory.GetPastOrderInfo().ToList<string>();
                int maxCount = orderInfo.GroupBy(t => t)
                               .OrderByDescending(t => t.Count())
                               .Select(t => t.Count())
                               .First();
                List<string> popularItem = orderInfo.GroupBy(t => t)
                                           .Where(t => t.Count() == maxCount)
                                           .Select(t => t.Key)
                                           .OrderBy(t => t)
                                           .ToList();
                if (popularItem.Count() == 0)
                    throw new Exception();
                return "Most popular item(s) is/are " + String.Join(',', popularItem);
            }
            catch(Exception)
            {
                return "No item is ordered till now!";
            }
        }

        // Warning: This method is intentionally added for testing purposes
        public static bool ClearHistory()
        {
            OrderHistory.GetInstance().ClearOrderHistory();
            return true;
        }
    }
}
