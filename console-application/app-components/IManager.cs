﻿namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // This interface contains the functionalities of a manager to be implemented
    internal interface IManager
    {
        string AddItemToMenu(string? itemName, string? itemPrice);
        string GetLastFiveOrders();
        string GetTheMostPopularItem();
        string GetItemWithHighestRevenue();
        string GetItemsBelowThePrice(string? price);
    }
}