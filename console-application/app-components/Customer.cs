﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDeliveryAppComponents.CustomerFunctionality
{
    // Customer class contains all the functonalities of a customer
    public sealed class Customer : ICustomer
    {
        // This method helps to place an order according to customer's choice
        public string PlaceOrder(string? itemChoice)  
        {
            OrderHandler orderHandler = new OrderHandler();
            orderHandler.OrderPlaced += OnOrderPlaced;
            return orderHandler.ProcessOrder(itemChoice ?? "0");
        }
        
        // It is an event handler method which raises itself on successfull placement of and order
        private void OnOrderPlaced(object source, OrderEventArgs args)
        {
            if(args.order.OrderStatus)
            {
                Console.WriteLine($"Order for {args.order.OrderName} placed successfully...");
            }
            else
            {
                Console.WriteLine("Order cannot be placed!!!");
            }
        }
    }
}
