﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace FoodDeliveryAppComponents.CustomerFunctionality
{
    // This class instantiates each and every order placed by the customer
    internal class Order
    {
        public readonly string? OrderName;
        public readonly bool OrderStatus;

        public Order(string? orderName, bool orderStatus)
        {
            OrderName = orderName;  
            OrderStatus = orderStatus;
            OrderHistory orderHistory = OrderHistory.GetInstance();
            if (orderName != null)
            {
                orderHistory.AddOrderInfo(orderName);
            }
        }
    }
}