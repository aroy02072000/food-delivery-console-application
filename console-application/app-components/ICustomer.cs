﻿namespace FoodDeliveryAppComponents.CustomerFunctionality
{
    // This interface contains the functionalities of a customer to be implemented
    internal interface ICustomer
    {
        string PlaceOrder(string itemChoice);
    }
}
