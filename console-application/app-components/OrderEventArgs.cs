﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDeliveryAppComponents.CustomerFunctionality
{
    // This class is used to wrap the Order object information while passing in an event
    internal class OrderEventArgs : EventArgs
    {
        public Order order { get; set; }
    }
}
