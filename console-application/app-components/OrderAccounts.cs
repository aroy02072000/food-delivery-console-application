﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // This class contains the temporary result set about the ordered items
    internal sealed class OrderAccounts
    {
        public string ItemName { get; set; }
        public double Revenue { get; set; }
        public int Qty { get; set; }
    }
}
