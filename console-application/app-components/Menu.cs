﻿namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // This singleton class contains the functionalities of a menu
    public sealed class Menu
    {
        public Dictionary<string, double> MenuList;
        private static Menu? ObjectExists = null;

        // Get a single instance of the class
        public static Menu GetInstance()
        {
            if(ObjectExists == null)
                ObjectExists = new Menu();
            return ObjectExists;
        }

        private Menu()
        {
           MenuList = new Dictionary<string, double>();
        }

        // This method can set the new item into existing menu
        internal void SetMenuItem(string itemName, double itemPrice)
        {
            MenuList[itemName] = itemPrice; 
        }

        // This method returns the menu
        public Dictionary<string, double> GetMenu()
        {
            return MenuList;
        }

        // This method is used to display the items available in menu
        public void ShowItems()
        {
            Menu menu = Menu.GetInstance();
            var menuList = menu.GetMenu();
            for (int i = 0; i < menuList.Count; i++)
            {
                Console.WriteLine($"Choice {i + 1} - {menuList.ElementAt(i)}");
            }
        }

        // Warning: This method is not the part of actual project
        // This method is intentionally added for testing purposes
        public bool ClearMenu()
        {
            MenuList.Clear();
            return true;    
        }
    }
}