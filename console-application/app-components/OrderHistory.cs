﻿namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // This singleton class is used to store the order history
    internal sealed class OrderHistory
    {
        public Stack<string> orderStack;

        private static OrderHistory? ObjectExists = null;

        // Get a single instance of the class
        public static OrderHistory GetInstance()
        {
            if(ObjectExists == null)   
                ObjectExists = new OrderHistory();
            return ObjectExists;
        }

        private OrderHistory()
        {
            orderStack = new Stack<string>();
        }

        // This method helps to store order information placed by customers
        public void AddOrderInfo(string itemName)
        {
            orderStack.Push(itemName);  
        }

        // This method helps to get the past order history
        public Stack<string> GetPastOrderInfo()
        {
            return orderStack;
        }

        // Warning: This method is not the part of actual project
        // This method is intentionally added for testing purposes
        public bool ClearOrderHistory()
        {
            orderStack.Clear();
            return true;
        }
        
    }
}
