﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace FoodDeliveryAppComponents.CustomerFunctionality
{
    // This class is used to handle the orders requested by customers
    internal class OrderHandler
    {
        internal event EventHandler<OrderEventArgs>? OrderPlaced;

        // This method is used to process the order requested by customer
        public string ProcessOrder(string itemChoice)
        {
            try
            {
                Menu menu = Menu.GetInstance();
                if (1 <= int.Parse(itemChoice) && int.Parse(itemChoice) <= menu.GetMenu().Count())
                {
                    string itemName = menu.GetMenu().ElementAt(int.Parse(itemChoice) - 1).ToString();
                    itemName = itemName.Substring(1, itemName.IndexOf(',') - 1);
                    Order orderObject = new Order(itemName, true);
                    OnOrderPlaced(orderObject);
                    return "Order placed successfully";
                }
                throw new Exception();
            }
            catch (Exception)
            {
                Order orderObject = new Order(null, false);
                OnOrderPlaced(orderObject);
                return "Item is not available";
            }
        }

        // Order event handler method
        protected virtual void OnOrderPlaced(Order orderObject)
        {
            if (OrderPlaced != null)
            {
                OrderPlaced(this, new OrderEventArgs() { order = orderObject});
            }
        }
    }
}