﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDeliveryAppComponents.ManagerFunctionality
{
    // This class contains the temporary result set
    internal sealed class ItemInfo
    {
        public string ItemName { get; set; }
        public double ItemPrice { get; set; }
    }
}
