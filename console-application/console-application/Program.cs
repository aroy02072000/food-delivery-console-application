﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodDeliveryAppComponents.CustomerFunctionality;
using FoodDeliveryAppComponents.ManagerFunctionality;

namespace ConsoleApp
{
    // Console Application Runner
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    char? choice = null;
                    Console.WriteLine("---------------------------------------------------");
                    Console.WriteLine("Select User Type (or close the application):");
                    Console.WriteLine("1. Customer");
                    Console.WriteLine("2. Manager");
                    Console.Write("Press 1 or 2: ");
                    choice = Console.ReadLine()[0];
                    switch (choice)
                    {
                        case '1':
                            Menu menu = Menu.GetInstance();
                            if (menu.GetMenu().Count == 0)
                            {
                                Console.WriteLine("There are no items to order!");
                                continue;
                            }
                            Customer customer = new Customer();
                            Console.WriteLine("Select an item to place order:");
                            menu.ShowItems();
                            Console.Write("Enter the item no.: ");
                            customer.PlaceOrder(Console.ReadLine());
                            continue;

                        case '2':
                            Manager manager = Manager.GetInstance();
                            Console.WriteLine("1. Add item to menu");
                            Console.WriteLine("2. Get last 5 orders");
                            Console.WriteLine("3. Get the most popular item");
                            Console.WriteLine("4. Get the item with highest revenue");
                            Console.WriteLine("5. Get items below the price");
                            Console.Write("Enter the choice: ");
                            char? funcChoice = Console.ReadLine()[0];
                            switch (funcChoice)
                            {
                                case '1':
                                    Console.Write("Enter the item name to be added: ");
                                    string? itemName = Console.ReadLine();
                                    Console.Write($"Enter the price of {itemName}: ");
                                    string? itemPrice = Console.ReadLine();
                                    Console.WriteLine(manager.AddItemToMenu(itemName, itemPrice));
                                    break;
                                case '2':
                                    Console.WriteLine(manager.GetLastFiveOrders());
                                    break;
                                case '3':
                                    Console.WriteLine(manager.GetTheMostPopularItem());
                                    break;
                                case '4':
                                    Console.WriteLine(manager.GetItemWithHighestRevenue());
                                    break; ;
                                case '5':
                                    Console.Write("Enter the maximum price limit: ");
                                    string? priceLimit = Console.ReadLine();
                                    Console.WriteLine(manager.GetItemsBelowThePrice(priceLimit));
                                    break;
                                default:
                                    Console.WriteLine("You've entered a wrong choice...");
                                    break;
                            }
                            continue;

                        default:
                            Console.WriteLine("You've entered a wrong choice...");
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}